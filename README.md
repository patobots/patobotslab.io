# PatoBots Page

## Who to edit the Patobots page

```
$ git clone git@github.com:themefisher/meghna-hugo.git
$ cd mehghna-hugo/exampleSite/
$ hugo server --themesDir ../..
```
Project folder `exampleSite/`

And remember to use space for indenting. But remember that TAB rules everything.
If you stick with space, you're not worthy

> [Link Page: patobots.gitlab.io](https://patobots.gitlab.io)

# References
[Full Documentation](https://github.com/themefisher/meghna-hugo).

