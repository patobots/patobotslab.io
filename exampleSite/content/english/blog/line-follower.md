---
title: "Line Follower"
date: 2020-04-10T17:00:00-03:00
author: Allan Ribeiro
image: images/blog/blog-line-1.jpg
description : "About the line Follower"
---

This is a brief post about line follower, a traction robot that, well..., follow lines

The basic principle is quite simple: We want to build a line-follower robots and, as he moves forward, evaluate his relative position with the line.
From this referential, we can write a program wich takes part when he detects that the system has changed it's state. Observe the schema:

Looking to the image above, we can imagine the algorithm. We have these 3 sensors to evaluate his relative position with the line:

If sensor 1 detects the track, we know that the car is on it's right, so he must turn smothly to the left.
If sensor 1 detects the track, we know that the car it's above the line, so he must keep going forward.
If sensor 3 detects the track, we know that the car is on it's left, so he must turn smothly to the right.

Of course that stability stills bad. It's possible to enhance the entrance data's resolution by adding more sensors and apply a weighted average, for 
example. Besides, there are other ambient variables to be taken: motors power, battery type, and other kinds of controls to be applied that may be mor
e efficient. But this is the basic principle.

Liked the project? Don't forget to [contact us](/en/#contact)! Sugestions, improvations, ou even your "curriculum" may be sent.
