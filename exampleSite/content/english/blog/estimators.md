---
title: "Estimating tasks weight"
date: 2021-03-18T17:00:00-03:00
author: Vitor O
image: images/blog/balanca.jpg
description : "Introduction on tasks evaluation in Agile method"
---

# Estimating weight of a task

------

How big the effort for finishing a task must be? The Agile evaluation is
one in many ways to find this answer. By using history points, for example,
we score a task and, as the scores rises, the effort involved grows as
well. Of course: to rate anything we must, first, turn ourselves to the
initial steps: the choose a scale, discussion of tasks and find a consensus
in the team.

![Image for post](https://miro.medium.com/max/818/0*QOhhh5EuxdAhu7fP.png)

One of the most popular agile scales is based in [Fibonacci
sequence](https://pt.wikipedia.org/wiki/Sequ%C3%AAncia_de_Fibonacci) to
assign points to efforts. Some people modify the Fibbonacci's sequence to a
larger one (1, 2, 3, 5, 8, 13, 20, 40 and 100 ). Using proximate numbers
might make it harder to distinguish between weights. Weber's law discuss
that.

It claims that we can distinguish between objects only in a certain
percentage of difference: in Fibonacci sequence, on average, each number is
60% higher than the previous one. And (see "Agile estimation") if we can
notice differences by 60% in effort between 2 tasks, point out differences
in others tasks is not any harder.

But in practice, we do not have any necessity of precision like 21
(blackjack!), so we round it to 20, or 25. Once deviated from Fibbonacci
original sequence, we are free to keep going. We may introduce 40 and 100.
A much bigger percentage growth (100 and 150 percent, respectivelly).

Now, imagine that your team's desire is to estimate the effort of building
a new prototype. Everyone agrees that the task would have a high level of
difficulty, and would take some time. And yet, imagine that this same team
used a scale as follows: 2, 4, 6, ..., 50. If everyone agrees, a new device
would be close to the superior limit.  But which value assign? 42? 48? The
higher the numer, on the scale, is, harder will become to choose the ideal
weight.

Keep in mind that the goal is just appraise the amount of effort. There is
no reason in trying to find a specific value.

The numbers are, in a way, a guide to help the evaluation of how much time
will take, and how many resources, you will need to devote. That's why no
viable Agile scale use decimals. Using Fibbonacci's sequence, to estimate
the effort on developing a prototype, we will have some numbers to choose
in the higher limit of the grade: 34, 55, 89... (This is where your Agile
Scale would stop).


Yet, there are more [methods of
rating](https://warren2lynch.medium.com/top-7-most-popular-agile-estimation-methods-for-user-stories-69fccf5e418e),
in case of you want to try. Search for the one will suit your team the
most. But remember to DOCUMENT.

## Sources 

------



1. [Agile Estimation: why the fibonacci sequence works
well for
estimating](https://www.mountaingoatsoftware.com/blog/why-the-fibonacci-sequence-works-well-for-estimating)
2. [Fibonacci agile
estimation](https://www.productplan.com/glossary/fibonacci-agile-estimation/)
3. [Medium: Top 7 most popular agile estimation methods
for users
stories](https://warren2lynch.medium.com/top-7-most-popular-agile-estimation-methods-for-user-stories-69fccf5e418e)
