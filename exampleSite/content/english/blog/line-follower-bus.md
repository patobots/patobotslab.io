---
title: "Following lines beyond competitions"
date: 2021-04-12T17:00:00-03:00
author: Vitor O
image: "images/blog/bus-line.jpg"
description : "Brief presentation of real applications for a line follower"
---

# Real applications of a line follower

------

When i entered PatoBots in 2018, i had to choose which cell i would enter.
Ended up choosing drone, but a query always followed me in these 3 years.
After all, how does a robot that follows line can be so simple, and yet so
complex at the same time?

As Allan already explained the bascis
[here](https://patobots.gitlab.io/blog/line-follower/), i will
make the things more interesting, and show some day-to-day applications
based in the line follower

In this article, entitled "[The Use of Computer Controlled Line
Follower Robots in Public Transport](https://www.sciencedirect.com/science/article/pii/S1877050916325704)"
, authors propose an adaptation of the robot for public transport, since
they already know the route (just like our little runner).

The use of other sensors would be necessary (as a [ultassonic
sensor](https://www.mecanicaindustrial.com.br/598-o-que-e-um-sensor-ultrassonico/))
that allow us to tell the distance between us and others veicles, and by
doing that, we would have controll in the current state of the vehicle,
like slowing down in rainy days, wait just the needed time to people
boarding and other funtions.

We could, yet, use the line follower in storage warehouses, to automate
cargo. Watch the video:

[![](http://img.youtube.com/vi/A8GM9T2Cgng/0.jpg)](http://www.youtube.com/watch?v=A8GM9T2Cgng
"")

Other uses include the one of guiding. Imagine that you are going to a
museum and wishes to see the exhibition in chronological order. The line
follower would already know the trajectory, and can make all the necessary
stops.

Of course it looks easy, but as the number of sensors grows, the robot
control becomes more complicated. Notice that we do not want the robot to
run over/bump into anyone during the course, so we need to guarantee that
he avoid colisions.

And what if the robots looses the track he was following?

Each case require specifics plans, to make decisions and avoid problems.
But the base is always the same.

And you? Already though of building a line follower?

Liked the project? Make sure to [contact us](/en/#contact).
Sugestions, criticisms, or even your "curriculum" can be sent.


## Sources

------

[Line Follower Robots – Controlling, Working Principle and Applications](https://www.elprocus.com/line-follower-robot-basics-controlling/#:~:text=Applications%20of%20line%20follower%20robot,on%20roads%20with%20embedded%20magnets)

[The Use of Computer Controlled Line Follower Robots in Public Transport](https://www.sciencedirect.com/science/article/pii/S1877050916325704)

