---
title: "The value of an hyphen/Selective Proccess"
date: 2021-03-18T20:00:00-03:00
author: Allan Ribeiro
image: images/blog/ps.png
description : "Selective proccess, first semester of 2021!"
---

Pandemic!

Amazing, isn't it? We were so fine in the beginning of 2020. PatoBots were
planning the last steps to start a new selective proccess. Just about to
launch a note, we had to safeguard ourselves, and all the projects, after
all, computers can be infected as well.

A simple example is the text insertion, intentional or not, into a
runtime code. Some compilers interpret everything, even comments (used to
build documentation in automated way). It is the case of "Shell Script"
[0000].

In platforms like Windows, Linux, Mac (or the commons, if you preffer), the
memory is protected at 7 keys by the system kernel: accessing such regions
will cause the infamous "Segmentation Fault"[0010].

But in microcontrolled platforms, mainly in developing stage, most people
does not implements. STM32[0011] has such lock, but it is deactivated by
default, for the developer tries to verify the code, by example, he needs
the locks open, to find error, verify logics, or reverse engineer it.

Those are examples in flowers world. Atacks based in such logic happens all
the time, and sometimes are easy to be made. Before we know the dome
exists, a certain rocket[0100] of a certain corporation fell, thanks to a
not seen, not intentional, hyphen in the code. When interpreted, in
runtime, caused fail in the coms between the rocket and command center, and
the trajectory corretion was ignored: the rocket began to turn during
flight. Luckly the deviation was in direction of a not inhabited area. The
rocket was then destructed. Lots of code fail like so, mainly when it comes
to microcontroler development.

0000 - [Shell script](https://en.wikipedia.org/wiki/Shell_script)

0001 - [Segmentation Fault](https://en.wikipedia.org/wiki/Segmentation_fault)

0010 - [STM32](https://pt.wikipedia.org/wiki/STM32)

0011 - [The most expensive hyphen in history](https://gizmodo.uol.com.br/hifen-nasa/)

Enough chitchat. I came to you to inform that we are resuming the SP, and
we like YOU to participate. So, i will leave here a copy of the note, and a
link to the form. Do not forget to check...

[Note](https://gitlab.com/patobots/admin/uploads/a574a8b98be41d209e8106fe7a1dc604/Processo_seletivo.odt)

[Form](https://forms.gle/XVgAGBtJHFBso58Y7)

Thaks for your attention - The presidency


You liked our project? Don't forget to [contact
us](/en/#contact). Sugestions, criticisms, or even your
"curriculum" can be sent.
