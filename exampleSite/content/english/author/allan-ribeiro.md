---
title: "Allan Ribeiro"
image: ""
email: "allanribeiro@alunos.utfpr.edu.br"
social:
  #- icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
  #- icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
  #- icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
---

Computing Engineering academic. Joined PatoBots in 2018, second semester. It's PatoBots vice-president and line-follower leader.
Interested in computational safety, Linux, control and automation, low-level programming, Futurama, puzzles, and coffee.
