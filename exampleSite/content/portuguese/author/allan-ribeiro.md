---
title: "Allan Ribeiro"
email: "allanribeiro@alunos.utfpr.edu.br"
social:
  #- icon : "ti-facebook" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
  #- icon : "ti-twitter-alt" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
  #- icon : "ti-github" # themify icon pack : https://themify.me/themify-icons
  #  link : "#"
---

Acadêmico de Engenharia da Computação. Entrou para a PatoBots no segundo semestre de 2018. É presidente da equipe e lider do seguidor de linhas.
Interessado em segurança computacional, Linux, controle e automação e programação de baixo nível, Futurama, puzzles, e café
