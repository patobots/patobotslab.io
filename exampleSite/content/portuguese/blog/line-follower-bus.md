---
title: "Seguindo linhas além das competições"
date: 2021-04-12T17:00:00-03:00
author: Vitor O
image: "images/blog/bus-line.jpg"
description : "Uma breve apresentação de aplicações reais para o seguidor de linha"
---


# Aplicações de um seguidor de linhas

------

Quando entrei na Patobots em 2018, tive que escolher qual célula desejava
fazer parte. Acabei por escolher o drone mas um dúvida sempre me seguiu
nesses quases 3 anos. Afinal, como um robô que segue uma linha pode ser
simples e complexo ao mesmo tempo?



Como o Allan ja explicou o básico
[aqui](https://patobots.gitlab.io/blog/line-follower/), vou
deixar as coisas mais interessantes e mostrar algumas aplicações do dia a
dia baseadas no seguidor de linha.



Neste artigo de 2016 intitulado ''[The Use of Computer Controlled Line
Follower Robots in Public
Transport](https://www.sciencedirect.com/science/article/pii/S1877050916325704)"
os autores propõe uma adaptação do seguidor de linha para o transporte
público, uma vez que estes possuem um rota já conhecida (assim como nossos
pequenos robôs). 

O uso de outros sensores acabaria sendo necessários (como um [sensor
ultrasônico](https://www.mecanicaindustrial.com.br/598-o-que-e-um-sensor-ultrassonico/)
que nos permitiria dizer a que distâncias estamos de outros veículos),
assim teriamos maior controle sobre o estado atual do veículo podendo
diminuir a velocidade em um dia de chuva, aguardar somente o tempo
necessário para embarque de passageiros e outras funções.



Podemos ainda utilizar o seguidor de linha em armazéns para o transporte de
carga automatizado. Veja no vídeo:

[![](http://img.youtube.com/vi/A8GM9T2Cgng/0.jpg)](http://www.youtube.com/watch?v=A8GM9T2Cgng
"")

Outra utilização que poderiamos dar a um seguidor é a de um guia. Imagine
que você vai a um museu e deseja conhecer uma exposição de acordo com a
ordem cronológicas das peças, então o seguidor ja sabendo o percurso guiará
você fazendo  as pausas necessárias.


É claro que falando assim parece tranquilo, mas quanto mais sensores temos
mais complexo fica o controle do robô. Veja não queremos que o robô
atropele/bata em ninguém durante o percurso, então precisamos garantir que
ele evite colisões. 

E se por acaso o robô perca a linha que está seguindo? 


Cada caso precisará de planejamentos específicos pensando em resolver
possívels tarefas e os problemas envolvidos nela, mas a base do seguidor de
linha permanece a mesma.


E você? Ja pensou em criar seu seguidor?


Gostou do projeto? Não deixe de nos [contatar]() Sugestões, críticas, ou
até mesmo seu "currículo" podem ser enviados


## Fontes

------

[Line Follower Robots – Controlling, Working Principle and Applications](https://www.elprocus.com/line-follower-robot-basics-controlling/#:~:text=Applications%20of%20line%20follower%20robot,on%20roads%20with%20embedded%20magnets)

[The Use of Computer Controlled Line Follower Robots in Public Transport](https://www.sciencedirect.com/science/article/pii/S1877050916325704)

