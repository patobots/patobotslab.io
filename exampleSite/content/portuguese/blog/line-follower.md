---
title: "Seguidor de Linhas"
date: 2020-10-04T17:00:00-03:00
author: Allan Ribeiro
image: images/blog/blog-line-1.jpg
description : "Sobre o seguidor de linhas"
---

Este é um breve post a respeito de seguidores de linhas, um robô de tração
que, bem..., segue linhas.

O princípio básico é simples: Queremos fazer um robô que segue linhas e,
enquanto se move para frente, avalie sua posição em relação à própria
linha.  A partir desse referencial, podemos programar o robô para tomar uma
decisão quando ele detectar que mudou de estado. Observe o esquema:


Olhando essa imagem acima, podemos começar a imaginar o algoritmo. Temos
esses 3 sensores para avaliar a posição dele em relação à linha. É simples:

Se o sensor 1 detectar a faixa, sabemos que o carrinho está à direita da
faixa, e deve virar levemente à esquerda.  Se o sensor 2 detectar a faixa,
estamos em cima da faixa, que é o que queremos. Vá para frente.  Se o
sensor 3 detectar a faixa, sabemos que o carrinho está à esquerda da faixa,
e deve virar levemente à direita.

Claro que a estabilidade ainda é pouca. Poderíamos melhorar a resolução da
entrada adicionando mais sensores e fazendo média ponderada, por exemplo.
Além disso há outras variáveis de ambiente a serem analisadas: potência dos
motores, tipo de bateria, e outros tipos de controle que podem se mostrar
mais eficientes. Mas este é o princípio básico.

Gostou do projeto? Não deixe de nos [contatar](/pt/#contact)
Sugestões, críticas, ou até mesmo seu "currículo" podem ser enviados

