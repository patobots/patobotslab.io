---
title: "Como robôs podem ser éticos"
date: 2021-05-27T17:00:00-03:00
author: Vitor O
image: "images/blog/ethical_robot.jpg"
description : "Ciência cognitiva e perspectivas éticas da mente"
---



Nos últimos anos vemos a crescente utilização da robótica, seja para uso
domiciliar, industrial ou até militar. E com o nível de autonomia desses
robôs crescendo torna-se obrigatório uma discussão sobre tomada de
decissões éticas. Essa necessidade é defendida por pesquisadores da área e
por pessoas como Stephen Hawking e Bill Gates por exemplo.



Atualmente a maioria das arquiteturas de controle robótica se utiliza de
uma abordagem em 3 camadas. Vejamos 


<p align="center">
 <img align="center" src=exampleSite/static/images/blog/3layerarch.jpg>
</p>




Com isso a abordagem utilizada para decissões éticas atualmente se resume
em verificar sentenças lógicas. Mas e se os robôs fossem capazes de simular
diferentes ações e suas consequências? 



Antes de continuar vamos falar um pouco sobre ciência cognitiva. Existem
algumas hipóteses de como a mente funciona e estaria organizada. Alguns
pesquisadores argumentam que nossa mente funcionaria além  de uma
manipulação de um sistema de simbolos abstratos, podemos entender essa
manipulação e sistema de simbolos como jogos infantis onde deve-se colocar
o quadrado no lugar  correspondente. Mas como eu disse nossa mente deve
lidar com informações de uma forma ainda mais complexa. E é aqui que entra
a hipostese da teoria da simulação cognitiva, nela ao se pensar sobre quais
ações tomar é preciso primeiro que façamos um modelo do ambiente (levando
em conta a complexidade do que está ao nosso redor). Não vou me alongar
mais nessa parte mas para os curiosos podem pesquisar pelo livro "How the
mind works" de Steven Pinker.



A proposta então é criar uma nova camada de controle para os robôs sendo
essa a camada ética. Nela o robô deve simular o seu  estado atual (posição
no espaço, sensores, ...) e futuro, bem como tentar prever o comportamente
de pessoas ao seu redor. E pensando nessas possíveis posições futuras (como
uma imaginação) deve decidir qual a melhor ação. 



Encontramos o nosso 2º problema nesse ponto, como eu sei qual é a melhor
ação?

Nesse ponto o debate cai no campo da ética e pode se extender bastante,
então pra fim de exemplos foi escolhida as 3 leis da robótica (criadas pelo
escritor Isaac Asimov). Vejamos:

* 1ª: Um robô não pode ferir um ser humano ou, por inação, permitir que um
  ser humano sofra algum mal.
* 2ª: Um robô deve obedecer as ordens que lhe sejam dadas por seres humanos
  exceto nos casos em que tais ordens entrem em conflito com a Primeira
Lei.
* 3ª: Um robô deve proteger sua própria existência desde que tal proteção
  não entre em conflito com a Primeira ou Segunda Leis.

 No estudo em que esse artigo se baseia os autores validaram as leis usando
4 experimentos e a métrica de quão seguro o robô e a pessoa ficariam ao fim
de um experimento. Os resultados foram satisfatórios e podem ser tidos como
o inicio de novas pesquisas no campo de ética robótica utilizando
imaginação funcional. Se ficou interessado o artigo é este aqui "[An
architecture for ethical robots inspired by the simulation theory of
cognition](https://doi.org/10.1016/j.cogsys.2017.04.002)" de 2018. 



O assunto e as dicussões são extensas, mas espero que tenha sido o
suficiente pra deixar a curiosidade surgir.


Gostou do projeto? Não deixe de nos [contatar](/pt/#contact) Sugestões,
críticas, ou até mesmo seu "currículo" podem ser enviados


