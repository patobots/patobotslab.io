---
title: "Estimando Peso de tarefas"
date: 2021-03-18T17:00:00-03:00
author: Vitor O
image: images/blog/balanca.jpg
description : "Introdução sobre estimativa de tarefas no método agile"
---


# Estimando o peso da tarefa

------

Qual é o esforço necessário para terminar uma tarefa? A estimativa ágil é
um meio de encontrar essa resposta. Por meio de pontos de história por
exemplo, pontuamos uma tarefa e quanto maior for sua pontuação, maior é o
esforço envolvido.  É claro que para fazer essa estimativa precisamos nos
atentar aos pontos iniciais: escolher uma escala, discutir as tarefas e
encontrar um consenso entre o time para a atribuir o peso de uma tarefa.

![Image for post](https://miro.medium.com/max/818/0*QOhhh5EuxdAhu7fP.png)

Uma escala bastante popular de estimativa ágil se baseia na [sequência de
Fibonacci](https://pt.wikipedia.org/wiki/Sequ%C3%AAncia_de_Fibonacci) para
atribuir as pontuações de esforços. É comum ainda que se use uma sequência
de Fibonacci modificada (1, 2, 3, 5, 8, 13, 20, 40 e 100 ).  Usar números
muito próximos uns dos outros torna a tarefa de distinguir entre as
estimativas quase impossível. Isso se deve à Lei de Weber. 

A Lei de Weber afirma que a diferença que podemos identificar entre os
objetos é dada por uma porcentagem.  Na sequência de Fibonacci vemos que
depois do 2 (que é 100% maior do que 1), cada número é cerca de 60% maior
do que o valor anterior.  E de acordo com a Lei de Weber (veja o artigo
"Agile Estimation" nas fontes), se pudermos distinguir uma diferença de 60%
no esforço entre duas estimativas, podemos distinguir essa mesma diferença
percentual entre outras estimativas.  Logo, os valores de Fibonacci
funcionam bem, pois aumentam aproximadamente na mesma proporção a cada vez.

Contudo na prática uma estimativa de 21 implica uma precisão que não
podemos suportar. É impressionante atribuir o peso 21 a uma tarefa e não
algum arredondamento como 20 ou ainda 25.  Isso nos leva a usar 20 em vez
de 21, e uma vez que desviamos da sequência de Fibonacci uma vez, nos
sentimos livres para continuar.  Podemos introduzir 40 e 100. Um aumento
muito maior do que 62% da sequência de Fibonacci (100% e 150%
respectivamente).

Agora imagine que sua equipe deseja estimar o esforço necessário para
construir um novo protótipo. Todos concordaram que essa tarefa teria um
alto nível de dificuldade e demoraria muito para ser concluída.  Mas
imagine que sua equipe usou uma escala de pontuação como: 2, 4, 6, ..., 50.
Se todos concordassem, um novo dispositivo estaria no limite superior da
escala de pontos, mas qual valor atribuir 42 pontos? 46? 48?  Quanto maior
é o número na escala, mais difícil se torna escolher o peso ideal.

Lembre-se de que o objetivo com esses pontos é apenas estimar o nível de
esforço. Não há razão para tentar escolher a pontuação perfeita de pontos. 

Os números são apenas um guia para ajudar sua equipe a avaliar quanto tempo
uma tarefa levará e quantos recursos você precisará dedicar a ela. É por
isso que nenhuma escala de estimativa ágil viável usa decimais.  Usando a
sequência de Fibonacci para estimar o esforço para desenvolver uma novo
protótipo, teremos alguns números para escolher na extremidade superior da
escala: 34, 55 ou 89. (É aqui que seu Fibonacci é ágil escala pararia.). 

Exitem ainda [outros métodos de
estimativa](https://warren2lynch.medium.com/top-7-most-popular-agile-estimation-methods-for-user-stories-69fccf5e418e),
caso queira teste e veja qual você e sua equipe se adaptará melhor, mas
lembre-se de documentar.

## Fontes 

------



1. [Agile Estimation: why the fibonacci sequence works well for estimating](https://www.mountaingoatsoftware.com/blog/why-the-fibonacci-sequence-works-well-for-estimating)
2. [Fibonacci agile estimation](https://www.productplan.com/glossary/fibonacci-agile-estimation/)
3. [Medium: Top 7 most popular agile estimation methods for users stories](https://warren2lynch.medium.com/top-7-most-popular-agile-estimation-methods-for-user-stories-69fccf5e418e)
