---
title: "O valor de um hífen/Processo Seletivo"
date: 2021-03-18T20:00:00-03:00
author: Allan Ribeiro
image: images/blog/ps.png
description : "Processo seletivo, primeiro semestre de 2021!"
---

Pandemia!

Incrível, não? Estávamos todos tão bem no início de 2020. A PatoBots
planejava os ultimos passos para dar início à um novo Processo Seletivo.
Prestes a lançar o edital, tivemos que resguardar todos os projetos, afinal
de contas, computadores também são infectáveis!

Um exemplo simples de infecção é a inserção de texto, intencional ou não,
em código durante o tempo de execução. Alguns compiladores interpretam
tudo, inclusive comentários, que são usados para produzir a documentação de
forma automatizada. É o caso do "Shell Script"[0000].

Em plataformas como Windows, Linux, Mac (ou seja, as mais comuns), a
memória é protegida, às 7 chaves, pelo kernel do sistema: tentar acessar
regiões que não reservaste causa o famoso "Segmentation Fault"[0010].

Mas em plataformas microcontroladas, principalmente em estágio de
desenvolvimento, a maioria não a implementa. O STM32[0011] possui tal trava, mas
vem desativada por padrão, pois quando o desenvolvedor tenta verificar o
código, por exemplo, ele precisa dessas trancas abertas, seja para
encontrar erros, verificar lógicas, ou fazer engenharia reversa.

Estes são exemplos no mundo das flores. Ataques baseados nesta lógica
acontecem o tempo todo, e muitas vezes são simples de fazer. Antes de
sabermos do domo, um certo foguete[0100] de uma certa empresa caiu, graças a
um hífen mal revisado no código. Quando interpretado, em tempo de execução,
causou falha na comunicação entre a base e o centro de controle, fazendo a
correção de trajetória ser ignorada, e o foguete virou em pleno ar. Pela
sorte, o desvio foi para uma área inabitada. O foguete foi em seguida
destruido. E muitos códigos falham assim, principalmente em se tratando de
desenvolvimento de microcontrolados.


0000 - [Shell script](https://en.wikipedia.org/wiki/Shell_script)

0001 - [Segmentation Fault](https://en.wikipedia.org/wiki/Segmentation_fault)

0010 - [STM32](https://pt.wikipedia.org/wiki/STM32)

0011 - [O hífen mais caro da história](https://gizmodo.uol.com.br/hifen-nasa/)

Chega de lero lero. Vim lhes informar que estamos retomando o PS, e
gostaríamos que participassem. Então vou deixar aqui uma cópia do edital,
um link para o formulário. Não se esqueçam de olhar...

[O edital](https://drive.google.com/file/d/1MoRqAgOiq8YDE4-aSPZ-VtqFeACqFaw5/view?usp=sharing)

[O Forumlário](https://forms.gle/XVgAGBtJHFBso58Y7)

Grato pela atenção - A Presidência


Gostou do projeto? Não deixe de nos [contatar](/pt/#contact) Sugestões, críticas, ou até mesmo seu "currículo" podem ser enviados
